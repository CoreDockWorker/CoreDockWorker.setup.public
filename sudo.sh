#!/usr/bin/env bash

############################
#  SUDO
############################
# chargement en direct des éléments nécessaires tools
timestamp=`date +%Y%m%d%H%M%S`
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh -O /tmp/.tools.${timestamp}.tmp && \
    source /tmp/.tools.${timestamp}.tmp && \
    rm /tmp/.tools.${timestamp}.tmp

if existing_command sudo; then
  echo 'sudo exists!'
else
  echo 'Your system does not have sudo'
  echo $DIST
  echo $DistroBasedOn
  PACKAGE_NAME[alpine]="sudo"
  PACKAGE_NAME[redhat]="sudo"
  PACKAGE_NAME[debian]="sudo"
  DistroUpdateCmd
  eval "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"
fi
