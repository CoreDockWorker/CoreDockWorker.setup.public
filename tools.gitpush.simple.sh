#!/usr/bin/env bash

# curl -O https://gitlab.com/CoreDockWorker/coredockworker.tools.public/raw/master/tools.gitpush.simple.sh && chmod +x tools.gitpush.simple.sh;

# This directory path
DIR="$(cd "$(dirname "$0")" && pwd -P)"
# Full path of this script
THIS="${DIR}/$(basename "$0")"

# Fonction pour vérifier et définir une valeur de configuration Git
check_and_set_git_config() {
  local config_key=$1
  local config_value=$2

  # Vérifie si la configuration est définie localement
  if git config --local "$config_key" >/dev/null 2>&1; then
    echo "La valeur '$config_key' est déjà définie localement : $(git config --local --get "$config_key")"
  # Vérifie si la configuration est définie globalement
  elif git config --global "$config_key" >/dev/null 2>&1; then
    echo "La valeur '$config_key' est déjà définie globalement : $(git config --global --get "$config_key")"
  # Si la configuration n'est pas définie localement ou globalement, la définir globalement
  else
    git config --global "$config_key" "$config_value"
    echo "La valeur '$config_key' a été définie globalement : $(git config --global --get "$config_key")"
  fi
}

# Définir la valeur de pull.rebase
check_and_set_git_config "pull.rebase" "true"
check_and_set_git_config "user.email" $(whoami)@$HOSTNAME
check_and_set_git_config "user.name" $(whoami)@$HOSTNAME

echo "**Les 3 valeurs Git indispensables ont été définies.**"


git fetch --all
git pull origin  $(git rev-parse --abbrev-ref HEAD)
git add --all
git commit --author="$(whoami) <$(whoami)@$HOSTNAME>" -am "$(whoami)@$HOSTNAME $(TZ="Europe/Paris" date +'%Y-%m-%d %H:%M %Z %A')"
git push origin  $(git rev-parse --abbrev-ref HEAD)
git pull origin  $(git rev-parse --abbrev-ref HEAD)
