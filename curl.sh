#!/usr/bin/env bash

############################
#  curl INSTALL
############################
# chargement en direct des éléments nécessaires tools
timestamp=`date +%Y%m%d%H%M%S`
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh -O /tmp/.tools.${timestamp}.tmp && \
    source /tmp/.tools.${timestamp}.tmp && \
    rm /tmp/.tools.${timestamp}.tmp

if existing_command curl; then
  echo 'curl exists!'
else
  echo 'Your system does not have curl'
  echo $DIST
  echo $DistroBasedOn
  declare -A PACKAGE_NAME
  PACKAGE_NAME["alpine"]="curl"
  PACKAGE_NAME["redhat"]="curl"
  PACKAGE_NAME["debian"]="curl"
  DistroUpdateCmd
  eval "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"
fi
if ! existing_command curl; then
  echo "ERROR: curl is not installed" | tee "/tmp/error.setup.log"
  exit 1
fi

############################
#  curl CONFIG
############################
