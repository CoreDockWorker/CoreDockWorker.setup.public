#!/usr/bin/env bash

OS=`uname | tr "[A-Z]" "[a-z]"`
if [ "${OS}" == "darwin" ]; then
    


## README
# /!\ Ce script d'installation est conçu pour mon usage. Ne le lancez pas sans vérifier chaque commande ! /!\

## La base : Homebrew
if test ! $(which brew)
then
	echo 'Installation de Homebrew'
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Vérifier que tout est bien à jour
brew update

## Utilitaires pour les autres apps : Cask et mas (Mac App Store)
echo 'Installation de mas, pour installer les apps du Mac App Store.'
brew install mas

echo 'Installation de Cask, pour installer les autres apps.'
brew tap caskroom/cask

# Installation d'apps avec mas (source : https://github.com/argon/mas/issues/41#issuecomment-245846651)
function install () {
	# Check if the App is already installed
	mas list | grep -i "$1" > /dev/null

	if [ "$?" == 0 ]; then
		echo "==> $1 est déjà installée"
	else
		echo "==> Installation de $1..."
		mas search "$1" | { read app_ident app_name ; mas install $app_ident ; }
	fi
}

echo 'Installation des apps : utilitaires.'
brew cask install appcleaner

echo 'Installation des apps : bureautique.'
install "OneDrive"
install "Microsoft Remote Desktop 10"
install "Microsoft OneNote"
brew cask install microsoft-teams
brew cask install microsoft-office
brew cask install adobe-creative-cloud


echo 'Installation des apps : communication.'
[ -e "/Applications/TeamViewer.app" ] || brew cask install teamviewer
[ -e "/Applications/Google Chrome.app" ] || brew cask install google-chrome
[ -e "/Applications/Firefox.app" ] || brew cask install firefox


## ************************* CONFIGURATION ********************************
echo "Configuration de quelques paramètres par défaut…"

## FINDER

# Affichage de la bibliothèque
chflags nohidden ~/Library

# Finder : affichage de la barre latérale / affichage par défaut en mode liste / affichage chemin accès / extensions toujours affichées
defaults write com.apple.finder ShowStatusBar -bool true
defaults write com.apple.finder FXPreferredViewStyle -string “Nlsv”
defaults write com.apple.finder ShowPathbar -bool true

# Afficher le dossier maison par défaut
defaults write com.apple.finder NewWindowTarget -string "PfHm"
defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/"

# Recherche dans le dossier en cours par défaut
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

# Coup d'œîl : sélection de texte
defaults write com.apple.finder QLEnableTextSelection -bool true

# Pas de création de fichiers .DS_STORE
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true


## APPS

## ************ Fin de l'installation *********
echo "Finder et Dock relancés… redémarrage nécessaire pour terminer."
killall Dock
killall Finder

# echo "Derniers nettoyages…"
# brew cleanup
# rm -f -r /Library/Caches/Homebrew/*

echo "ET VOILÀ !"

fi