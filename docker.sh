#!/usr/bin/env bash

export VERBOSE=1

############################
#  docker
############################
# chargement en direct des éléments nécessaires tools
timestamp=`date +%Y%m%d%H%M%S`
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh -O /tmp/.tools.${timestamp}.tmp && \
    source /tmp/.tools.${timestamp}.tmp && \
    rm /tmp/.tools.${timestamp}.tmp

if existing_command docker; then
  echo 'docker exists!'
elif [ "${OS}" == "mac" ]; then
    echo 'installer docker sur mac avec brew !'
    echo 'brew cask install docker'
else
    #install docker
    curl -fsSL get.docker.com -o get-docker.sh && chmod 777 get-docker.sh
    ( exec "./get-docker.sh" 2> "/tmp/install.docker.log" > "/tmp/install.docker.log" ) 
    rm get-docker.sh
    usermod -aG docker $(logname)
    usermod -aG docker $USER
    if ! existing_command docker; then
      echo "ERROR: docker is not installed" | tee "/tmp/error.setup.log"
      exit 1
    fi
    docker --version
fi