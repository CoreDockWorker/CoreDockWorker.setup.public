#!/bin/sh

# wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/setup.sh -O setup.sh && chmod +x setup.sh
# curl -O https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/setup.sh && chmod +x setup.sh
# ./setup.sh

# Vérifie si l'utilisateur courant est root
if [ "$EUID" -ne 0 ]; then
  echo "Ce script doit être exécuté en tant que root (avec sudo si possible)."
  exit 1
fi

echo "Le script est exécuté en tant que root."

# define VERBOSE=1 to echoed logging
[ -z ${VERBOSE+x} ] && VERBOSE=0 && export VERBOSE
# define ERROR_LOG_FILE to echoed get error 
[ -z ${ERROR_LOG_FILE+x} ] && ERROR_LOG_FILE="/tmp/error.setup.log" && export ERROR_LOG_FILE && rm -f $ERROR_LOG_FILE

REPOSITORY="https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public.git"
DIRECTORY="CoreDockWorker/CoreDockWorker.setup.public"

############################
#  1) WGET sur MAC
############################
OS=`uname | tr "[A-Z]" "[a-z]"`
if [ "${OS}" = "darwin" ]; then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew install wget
fi

############################
#  1) install GIT & CURL
############################
# chargement en direct des éléments nécessaires ssh, git et curl
timestamp=`date +%Y%m%d%H%M%S`

if [ ! -f "ssh.sh" ] || [ ! -f "git.sh" ] || [ ! -f "curl.sh" ]; then 
    wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/ssh.sh  -O /tmp/.ssh.${timestamp}.tmp  && chmod +x /tmp/.ssh.${timestamp}.tmp
    wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/git.sh  -O /tmp/.git.${timestamp}.tmp  && chmod +x /tmp/.git.${timestamp}.tmp
    wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/curl.sh -O /tmp/.curl.${timestamp}.tmp && chmod +x /tmp/.curl.${timestamp}.tmp
else  
    cp ssh.sh  /tmp/.ssh.${timestamp}.tmp  && chmod +x /tmp/.ssh.${timestamp}.tmp
    cp git.sh  /tmp/.git.${timestamp}.tmp  && chmod +x /tmp/.git.${timestamp}.tmp
    cp curl.sh /tmp/.curl.${timestamp}.tmp && chmod +x /tmp/.curl.${timestamp}.tmp
fi
if ! ( exec "/tmp/.ssh.${timestamp}.tmp"  2>> $ERROR_LOG_FILE ); then echo "ssh NOK"  && exit 1; fi
if ! ( exec "/tmp/.git.${timestamp}.tmp"  2>> $ERROR_LOG_FILE ); then echo "git NOK"  && exit 1; fi
if ! ( exec "/tmp/.curl.${timestamp}.tmp" 2>> $ERROR_LOG_FILE ); then echo "curl NOK" && exit 1; fi
rm -f /tmp/.ssh.${timestamp}.tmp
rm -f /tmp/.git.${timestamp}.tmp
rm -f /tmp/.curl.${timestamp}.tmp

############################
#  EXECUTING SCRIPTS
############################

# working home directory allways
cd ~/

# clone project for retrieving scripts
[ ! -d $DIRECTORY ] && \
  git clone $REPOSITORY $DIRECTORY
[ -d $DIRECTORY  ] && cd $DIRECTORY && git fetch --all && git pull

# launch minimal scripts for alpine
for file in ./alpine/*.sh; do
  [ -f "$file" ] && \
  [ -x "$file" ] && \
    ( exec "${file}" )
done

# launch all scripts
for file in ./*sh; do
    if echo $file | grep -q "setup"; then
        echo -e "\033[1;33m$file not executed again\033[0m"
    elif echo $file | grep -q "tools."; then
        echo -e "\033[1;33m$file is a tools file\033[0m"
    elif [ ! -f "$file" ]; then 
        echo -e "\033[1;33m$file not a file\033[0m" 
    elif [ ! -x "$file" ]; then
        echo -e "\033[1;33m$file not executable\033[0m"
    else
        echo -e "\033[0;32mexecute "$file"\033[0m"
        if ! ( exec "${file}" 2>> "/tmp/${file}.log" ); then 
            echo -e "\033[0;31mERROR executing "$file"\033[0m"
            cat "/tmp/${file}.log" >> $ERROR_LOG_FILE
        fi
    fi
done

[ -f "$ERROR_LOG_FILE" ] && \
  [ ! `cat $ERROR_LOG_FILE | wc -l` = 0 ] && \
  echo -e "\033[0;31mERROR: setup problem behind\033[0m" && \
  echo -e "\033[0;31m see error file : $ERROR_LOG_FILE\033[0m" && \
  cat $ERROR_LOG_FILE && \
  exit 1

exit 0
