# Brief 

* installation de git et des préférences

# postinstallation script pour debian

<pre>
wget https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/setup.sh
sudo bash ./setup.sh
rm ./setup.sh
</pre>

# postinstallation script pour Mac OS X

* lancement du script profile_maj.sh dans le terminal

<pre>
curl -O https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/setup.sh && chmod +x setup.sh
bash ./setup.sh
rm ./setup.sh
</pre>

# postinstallation script pour raspberry

<pre>
wget https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/setup.sh
bash ./setup.sh
rm ./setup.sh
</pre>

# postinstallation script pour kimsufi DEBIAN 8

## 1. Sélection template

  * Choisir le template Debian dernière version (ici 9.3)
  * Laisser Anglais (inutile de passer un serveur en Français)
  * Cocher Installation personnalisée
  * Cliquer sur Suivant

![alt text](README.images/01-selection-template.png "selection template")

## 2. Partitionnement

  * Pour le partitionnement :
  * supprimer la partition /home
  * éditer la partition / et cocher utiliser l'espace restant
  * voir les captures ci-dessous
  * Cliquer sur Suivant

![alt text](README.images/02-partition-supprimer-home.png "selection template")
![alt text](README.images/02a-partition-agrandir-racine.png "selection template")
![alt text](README.images/02b-partition-final.png "selection template")

## 3. Options et postinstallation

  * Remplir le nom d'hôte de votre serveur
  * Sélectionner votre clé SSH
  * coller le script de post installation suivant : 
  * https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/setup.sh
  * Cliquer sur Suivant

![alt text](README.images/03-options-avec-postinstallation.png "selection template")

## 4. confirmer et attendre
