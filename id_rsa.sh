#!/usr/bin/env bash

############################
#  ID_RSA INSTALL
############################
# chargement en direct des éléments nécessaires tools
timestamp=$(date +%Y%m%d%H%M%S)
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh -O /tmp/.tools.${timestamp}.tmp && \
    source /tmp/.tools.${timestamp}.tmp && \
    rm /tmp/.tools.${timestamp}.tmp

if ! existing_command ssh-keygen; then
  echo "ERROR: ssh-keygen is not installed"
  exit 1
fi

############################
#  ID_RSA CONFIG
############################

# Déterminer le vrai utilisateur, même sous sudo
REAL_USER=${SUDO_USER:-$USER}
REAL_HOME=$(eval echo ~$REAL_USER)

# Créer le répertoire .ssh s'il n'existe pas
mkdir -p $REAL_HOME/.ssh

[ ! -f $REAL_HOME/.ssh/id_rsa ] && \
  sudo -u $REAL_USER ssh-keygen -t rsa -b 4096 -C "$REAL_USER@$HOSTNAME" -f $REAL_HOME/.ssh/id_rsa -q -P "" 
[ ! -f $REAL_HOME/.ssh/id_rsa ] && \
  echo "ERROR: id_rsa problem" && \
  exit 1
  
echo "id_rsa OK"
# cat $REAL_HOME/.ssh/id_rsa.pub
exit 0