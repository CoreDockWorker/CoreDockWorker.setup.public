#!/bin/sh


# define ERROR_LOG_FILE to echoed get error
[ -z ${ERROR_LOG_FILE+x} ] && ERROR_LOG_FILE="/tmp/error.setup.log" && export ERROR_LOG_FILE

############################
#  BASH
############################
if [ -f /etc/alpine-release ]; then
	if [ ! `command -v bash` ]; then
		apk update
		apk add bash 
	fi
fi
if [ ! `command -v bash` ]; then
	echo "ERROR: bash is not installed" | tee "$ERROR_LOG_FILE"
	exit 1
fi
