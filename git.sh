#!/usr/bin/env bash

############################
#  GIT INSTALL
############################
# chargement en direct des éléments nécessaires tools
timestamp=`date +%Y%m%d%H%M%S`
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh -O /tmp/.tools.${timestamp}.tmp && \
    source /tmp/.tools.${timestamp}.tmp && \
    rm /tmp/.tools.${timestamp}.tmp

if existing_command git; then
  echo 'git exists!'
else
  echo 'Your system does not have git'
  echo $DIST
  echo $DistroBasedOn
  declare -A PACKAGE_NAME
  PACKAGE_NAME[alpine]="git"
  PACKAGE_NAME[redhat]="git"
  PACKAGE_NAME[debian]="git"
  DistroUpdateCmd
  echo "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"
  eval "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"
fi
if ! existing_command git; then
  echo "ERROR: git is not installed" | tee "/tmp/error.setup.log"
  exit 1
fi


############################
#  GIT CONFIG
############################

git config --global user.email "$USER@$HOSTNAME"
git config --global user.name  "$USER@$HOSTNAME"

git config --global push.default simple

# add ssk key from gitlab.com
mkdir -p ~/.ssh
ssh-keyscan -t rsa,dsa gitlab.com  >> ~/.ssh/known_hosts 2> /dev/null

