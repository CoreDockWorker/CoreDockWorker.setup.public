#!/bin/bash

############################
# HOSTNAME REVERSE 
############################

[ ! -f tools.inc.sh ] && curl -O https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh
source tools.inc.sh

reverse=0;

for ((i=1;i<=$#;i++)); 
do
  if [ $1 = "--reverse" ]; 
  then ((i++)) 
    reverse=1;
  fi
done;
if [ $reverse = 0 ]; then
  printing_color_load
  echo -e "$LightBlue Usage : hostname.sh --reverse$NC"
  echo -e "$LightBlue To define hostname to reverse dns$NC"
  exit 0
fi

[ ! -f sudo.sh ] && curl -O https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/sudo.sh
source sudo.sh

# TODO AMELIORER L'INSTALLATION DE HOSTNAMECTL et HOST
if existing_command host; then
  echo 'host exists!'
else
  echo 'Your system does not have host'
  echo $DIST
  echo $DistroBasedOn
  # PACKAGE_NAME[alpine]="bind-tools" # ne fonctionne pas sur alpine
  PACKAGE_NAME[redhat]="host"
  PACKAGE_NAME[debian]="host"
  DistroUpdateCmd
  eval "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"
fi

myip=`curl http://ipecho.net/plain 2> /dev/null ; echo`
echo $myip
myreversehost=`host $myip | rev | cut -d\  -f1 | cut -c 2- | rev`
echo $myreversehost

sudo hostnamectl set-hostname $myreversehost
hostnamectl
