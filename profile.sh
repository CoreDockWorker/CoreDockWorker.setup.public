#!/bin/bash
# if not root, run as root
if (( $EUID != 0 )); then
    sudo $0
    exit
fi

wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/profile.sh -O profile.sh && chmod +x profile.sh
# ./profile.sh

mkdir -p /etc/profile.d/
    
OS=`uname | tr "[A-Z]" "[a-z]"`
if [ "${OS}" == "darwin" ]; then

    # chargement librairie insert or replace
    timestamp=`date +%Y%m%d%H%M%S`
    curl -s https://gitlab.com/CoreDockWorker/coredockworker.tools.public/raw/master/tools.function.insertorreplace.sh -o /tmp/.myscript.${timestamp}.tmp
    source /tmp/.myscript.${timestamp}.tmp
    rm -f /tmp/.myscript.${timestamp}.tmp
    
    INSERTORREPLACEFILE="/tmp/.inputrc.${timestamp}.tmp"
    STARTCHAIN="## START profile.d"
    ENDCHAIN="## END profile.d"
    FILE="/etc/profile"
    echo 'for sh in /etc/profile.d/*.sh ; do
        [ -r "$sh" ] && . "$sh"
done
unset sh' > "${INSERTORREPLACEFILE}"


    insertorreplace_infile_betweentwochains_quiet "${INSERTORREPLACEFILE}" "${STARTCHAIN}" "${ENDCHAIN}" "${FILE}"
    rm -f ${INSERTORREPLACEFILE}
    if [ ! -f ~/.bashrc ]; then
        echo ". /etc/profile" > ~/.bashrc
    fi
    
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    
fi

wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/profile.d/01.ls.sh    -O /etc/profile.d/01.ls.sh
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/profile.d/02.PS1.sh   -O /etc/profile.d/02.PS1.sh
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/profile.d/03.du.sh    -O /etc/profile.d/03.du.sh
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/profile.d/04.ps.sh    -O /etc/profile.d/04.ps.sh
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/profile.d/05.myip.sh  -O /etc/profile.d/05.myip.sh
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/profile.d/06.completion.sh  -O /etc/profile.d/06.completion.sh
chmod +x /etc/profile.d/*.sh

