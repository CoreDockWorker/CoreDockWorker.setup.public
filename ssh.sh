#!/usr/bin/env bash

############################
#  ssh INSTALL
############################
# chargement en direct des éléments nécessaires tools
timestamp=`date +%Y%m%d%H%M%S`
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh -O /tmp/.tools.${timestamp}.tmp && \
    source /tmp/.tools.${timestamp}.tmp && \
    rm /tmp/.tools.${timestamp}.tmp

if existing_command ssh; then
  echo 'ssh exists!'
else
  echo 'Your system does not have ssh'
  echo $DIST
  echo $DistroBasedOn
  declare -A PACKAGE_NAME
  PACKAGE_NAME["alpine"]="openssh"
  PACKAGE_NAME["redhat"]="openssh-clients"
  PACKAGE_NAME["debian"]="ssh"
  DistroUpdateCmd
  eval "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"
fi
if ! existing_command ssh; then
  echo "ERROR: ssh is not installed" | tee "/tmp/error.setup.log"
  exit 1
fi

############################
#  ssh CONFIG
############################

mkdir -p ~/.ssh
