#!/usr/bin/env bash

timestamp=`date +%Y%m%d%H%M%S`
INSERTORREPLACEFILE="/tmp/.inputrc.${timestamp}.tmp"
STARTCHAIN="## START completion-ignore-case"
ENDCHAIN="## END completion-ignore-case"
FILE="$HOME/.inputrc"

if [ ! -e  ${FILE} ] || ! grep -q "${STARTCHAIN}" ${FILE} ; then
    # chargement librairie insert or replace
    curl -s https://gitlab.com/CoreDockWorker/coredockworker.tools.public/raw/master/tools.function.insertorreplace.sh -o /tmp/.myscript.${timestamp}.tmp
    source /tmp/.myscript.${timestamp}.tmp
    rm -f /tmp/.myscript.${timestamp}.tmp
    
    echo 'set completion-ignore-case On' > "${INSERTORREPLACEFILE}"
    echo "modification de $FILE"
    
    insertorreplace_infile_betweentwochains_quiet "${INSERTORREPLACEFILE}" "${STARTCHAIN}" "${ENDCHAIN}" "${FILE}"
    rm -f ${INSERTORREPLACEFILE}

fi
