#!/usr/bin/env bash

# SUIVRE CETTE DOCUMENTATION 
# https://github.com/astrada/google-drive-ocamlfuse/wiki/Installation
# et cette documentation 
# https://github.com/astrada/google-drive-ocamlfuse/wiki/Headless-Usage-&-Authorization

OS=`uname | tr "[A-Z]" "[a-z]"`
if [ "${OS}" == "linux" ]; then
    

[ ! -f tools.inc.sh ] && curl -O https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh
source tools.inc.sh

reverse=0;

for ((i=1;i<=$#;i++)); 
do
  if [ $1 = "--install" ]; 
  then ((i++)) 
    reverse=1;
  fi
done;
if [ $reverse = 0 ]; then
  printing_color_load
  echo -e "$LightBlue Usage : google-drive-ocamlfuse.sh --install"
  echo -e "$LightBlue To install all tools google-drive-ocamlfuse$NC"
  exit 0
fi

############################
#  google-drive-ocamlfuse INSTALL
############################
# chargement en direct des éléments nécessaires tools
timestamp=`date +%Y%m%d%H%M%S`
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh -O /tmp/.tools.${timestamp}.tmp && \
    source /tmp/.tools.${timestamp}.tmp && \
    rm /tmp/.tools.${timestamp}.tmp

if existing_command google-drive-ocamlfuse; then
  echo 'google-drive-ocamlfuse exists!'
else
  echo 'Your system does not have google-drive-ocamlfuse'
  echo $DIST
  echo $DistroBasedOn
  declare -A PACKAGE_NAME
  PACKAGE_NAME["alpine"]=""
  PACKAGE_NAME["redhat"]=""
  PACKAGE_NAME["debian"]="software-properties-common dirmngr"
  DistroUpdateCmd
  eval "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"

  add-apt-repository -y ppa:alessandro-strada/ppa
  apt-key adv --keyserver keyserver.ubuntu.com --recv-keys AD5F235DF639B041
  
  
  PACKAGE_NAME["alpine"]="google-drive-ocamlfuse"
  PACKAGE_NAME["redhat"]="google-drive-ocamlfuse"
  PACKAGE_NAME["debian"]="google-drive-ocamlfuse"
  DistroUpdateCmd
  eval "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"

fi
if ! existing_command google-drive-ocamlfuse; then
  echo "ERROR: google-drive-ocamlfuse is not installed" | tee "/tmp/error.setup.log"
  exit 1
fi
fi
