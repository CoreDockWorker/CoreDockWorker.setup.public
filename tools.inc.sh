#!/usr/bin/env bash

function existing_variable()
{
  [ ! -z ${!1+x} ]
  # EXAMPLE
  # if existing_variable VERBOSE; then
  #  echo 'VERBOSE exists!'
  # else
  #  echo 'VERBOSE does not exist'
  # fi
}
export -f existing_variable

# define VERBOSE=1 to echoed logging
if existing_variable VERBOSE; then VERBOSE=0 && export VERBOSE; fi

function logging () {
  if [ -n "$1" ]
  then
      IN="$1"
  else
      read IN # This reads a string from stdin and stores it in a variable called IN
  fi
  if [ "$VERBOSE" = 1 ]; then
    echo -e $IN
  fi
}
export -f logging
function printing () {
  if [ -n "$1" ]
  then
      IN="$1"
  else
      read IN # This reads a string from stdin and stores it in a variable called IN
  fi
  echo -e "$IN"
}
export -f printing
function printing_color_load () {
  readonly Black='\033[0;30m'
  export DarkGray='\033[1;30m'
  export Red='\033[0;31m'
  export LightRed='\033[1;31m'
  export Green='\033[0;32m'
  export LightGreen='\033[1;32m'
  export BrownOrange='\033[0;33m'
  export Yellow='\033[1;33m'
  export Blue='\033[0;34m'
  export LightBlue='\033[1;34m'
  export Purple='\033[0;35m'
  export LightPurple='\033[1;35m'
  export Cyan='\033[0;36m'
  export LightCyan='\033[1;36m'
  export LightGray='\033[0;37m'
  export White='\033[1;37m'
  export NC='\033[0m'
}
export -f printing_color_load
function existing_command()
{
  command -v "$1" >/dev/null 2>&1
  # EXAMPLE
  # if existing_command bash; then
  #  echo 'Bash exists!'
  # else
  #  echo 'Your system does not have Bash'
  # fi
}
export -f existing_command

function existing_file()
{
  [ -f "$1" ]
  # EXAMPLE
  # if existing_file bash.sh; then
  #  echo 'file bash.sh exists!'
  # else
  #  echo 'file bash.sh does not exists'
  # fi
}
export -f existing_file
function lowercase()
{
	echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}
export -f lowercase
shootProfile(){
	OS=`lowercase \`uname\``
	KERNEL=`uname -r`
	MACH=`uname -m`

	if [ "${OS}" == "windowsnt" ]; then
		OS=windows
	elif [ "${OS}" == "darwin" ]; then
		OS=mac
	else
		OS=`uname`
		if [ "${OS}" = "SunOS" ] ; then
			OS=Solaris
			ARCH=`uname -p`
			OSSTR="${OS} ${REV}(${ARCH} `uname -v`)"
		elif [ "${OS}" = "AIX" ] ; then
			OSSTR="${OS} `oslevel` (`oslevel -r`)"
		elif [ "${OS}" = "Linux" ] ; then
			if [ -f /etc/redhat-release ] ; then
				DistroBasedOn='RedHat'
				DistroInstallCmd='yum install -y'
				# update if there is no update since 5 minutes
				DIST=`cat /etc/redhat-release |sed s/\ release.*//`
				PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/SuSE-release ] ; then
				DistroBasedOn='SuSe'
				PSUEDONAME=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
				REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
			elif [ -f /etc/mandrake-release ] ; then
				DistroBasedOn='Mandrake'
				PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/debian_version ] ; then
				DistroBasedOn='Debian'
				DistroInstallCmd='DEBIAN_FRONTEND=noninteractive apt-get install -qy'
				# update if there is no update since 5 minutes
				if [ -f /etc/lsb-release ] ; then
			        	DIST=`cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }'`
			                PSUEDONAME=`cat /etc/lsb-release | grep '^DISTRIB_CODENAME' | awk -F=  '{ print $2 }'`
			                REV=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }'`
            			fi
			elif [ -f /etc/alpine-release ] ; then
                                DistroBasedOn='Alpine'
				DistroInstallCmd='apk add '
				DIST=`cat /etc/alpine-release`
			elif [ -f /etc/os-release ] && [ -n "echo /etc/os-release | grep coreos"  ] ; then
				DistroBasedOn='coreos'
			fi
			if [ -f /etc/UnitedLinux-release ] ; then
				DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
			fi
			OS=`lowercase $OS`
			DistroBasedOn=`lowercase $DistroBasedOn`
		 	export OS
		 	export DIST
		 	export PSUEDONAME
		 	export REV
		 	export KERNEL
		 	export MACH
		fi

	fi
}
shootProfile
export -f shootProfile
function DistroUpdateCmd () {
  if [ $DistroBasedOn = "debian"  ]; then
    [ $(find /var/lib/apt -mmin -5 | wc -l) -eq "0" ] && \
      DEBIAN_FRONTEND=noninteractive apt-get update -y 2> /dev/null && \
      DEBIAN_FRONTEND=noninteractive apt-get install -qy apt-utils 2> /dev/null
  elif [ $DistroBasedOn = "alpine"  ]; then
    apk update
  elif [ $DistroBasedOn = "redhat"  ]; then
    [ `find /var/lib/yum -mmin -5 | wc -l` = 0 ] && \
      yum update -y
  fi
}
export -f DistroUpdateCmd
