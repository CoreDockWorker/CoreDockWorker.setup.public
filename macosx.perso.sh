#!/usr/bin/env bash


# curl -O https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/macosx.perso.sh && chmod +x macosx.perso.sh
# ./macosx.perso.sh --install
# ensuite
# brew update && brew upgrade && brew cask upgrade

OS=`uname | tr "[A-Z]" "[a-z]"`
if [ "${OS}" == "darwin" ]; then

[ ! -f tools.inc.sh ] && curl -O https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh
source tools.inc.sh

reverse=0;

for ((i=1;i<=$#;i++)); 
do
  if [ $1 = "--install" ]; 
  then ((i++)) 
    reverse=1;
  fi
done;
if [ $reverse = 0 ]; then
  printing_color_load
  echo -e "$LightBlue Usage : macosx.sh --install"
  echo -e "$LightBlue To install all tools macosx$NC"
  exit 0
fi



## README
# /!\ Ce script d'installation est conçu pour mon usage. Ne le lancez pas sans vérifier chaque commande ! /!\

## La base : Homebrew
if test ! $(which brew)
then
	echo 'Installation de Homebrew'
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Vérifier que tout est bien à jour
brew update

## Utilitaires pour les autres apps : Cask et mas (Mac App Store)
echo 'Installation de mas, pour installer les apps du Mac App Store.'
brew install mas

echo 'Installation de Cask, pour installer les autres apps.'
brew tap caskroom/cask
brew tap homebrew/cask-drivers
brew install brew-cask-completion


# Installation d'apps avec mas (source : https://github.com/argon/mas/issues/41#issuecomment-245846651)
function install () {
	# Check if the App is already installed
	mas list | grep -i "$1" > /dev/null

	if [ "$?" == 0 ]; then
		echo "==> $1 est déjà installée"
	else
		echo "==> Installation de $1..."
		mas search "$1" | { read app_ident app_name ; mas install $app_ident ; }
	fi
}

echo 'Installation des apps : utilitaires.'
[ -e "/Applications/AppCleaner.app" ] && echo "==> AppCleaner est déjà installée" || brew cask install appcleaner
[ -e "/Applications/BBEdit.app" ] && echo "==> BBEdit est déjà installée" || brew cask install bbedit
[ -e "/Applications/Google Drive File Stream.app" ] && echo "==> Google Drive File Stream est déjà installée" || brew cask install google-drive-file-stream
[ -e "/Applications/OnyX.app" ] && echo "==> Onyx est déjà installée"  || brew cask install onyx
[ -e "/Applications/Disk Inventory X.app" ] && echo "==> Disk Inventory X est déjà installée"  || brew cask install disk-inventory-x
[ -e "/Applications/OmniDiskSweeper.app" ] && echo "==> OmniDiskSweeper est déjà installée"  || brew cask install OmniDiskSweeper
[ -e "/Applications/Bitwarden.app" ] && echo "==> Bitwarden est déjà installée"  || brew cask install bitwarden
[ -e "/Applications/Calibre.app" ] && echo "==> Calibre est déjà installée"  || brew cask install calibre
[ -e "/Applications/Steam.app" ] && echo "==> Steam est déjà installée"  || brew cask install steam

echo 'Installation des apps : bureautique.'
install "Pages"
install "Keynote"
install "Numbers"
install "Xcode"

[ -e "/Applications/LibreOffice.app" ] && echo "==> Libreoffice est déjà installée"  || brew cask install libreoffice
[ -e "/Applications/OpenOffice.app" ] && echo "==> OpenOffice est déjà installée"  || brew cask install openoffice
[ -e "/Applications/MySQLWorkbench.app" ] && echo "==> MySQLWorkbench est déjà installée"  || brew cask install mysqlworkbench
[ -e "/Applications/Tunnelblick.app" ] && echo "==> Tunnelblick est déjà installée"  || brew cask install tunnelblick

echo 'Installation des apps : développement.'
[ -e "/Applications/iTerm.app" ] && echo "==> iTerm est déjà installée"  || brew cask install iterm2
[ -e "/Applications/Gimp.app" ] && echo "==> Gimp est déjà installée"  || brew cask install gimp
[ -e "/Applications/MacPass.app" ] && echo "==> MacPass est déjà installée"  || brew cask install macpass
[ -e "/Applications/Angry IP Scanner.app" ] && echo "==> Angry IP Scanner est déjà installée"  || brew cask install java && brew cask install angry-ip-scanner
[ -e "/Applications/Spotify.app" ] && echo "==> Spotify est déjà installée"  || brew cask install spotify
[ -e "/Applications/MediaElch.app" ] && echo "==> MediaElch est déjà installée"  || brew cask install mediaelch
[ -e "/Applications/Transmission Remote GUI.app" ] && echo "==> Transmission Remote GUI est déjà installée"  || brew cask install transmission-remote-gui
[ -e "/Applications/VLC.app" ] && echo "==> VLC est déjà installée"  || brew cask install vlc
[ -e "/Applications/HSTracker.app" ] && echo "==> HSTracker est déjà installée"  || brew cask install hstracker

echo 'Installation des apps : communication.'
[ -e "/Applications/TeamViewer.app" ] && echo "==> TeamViewer est déjà installée"  || brew cask install teamviewer
[ -e "/Applications/nextcloud.app" ] && echo "==> nextcloud est déjà installée"  || brew cask install nextcloud
[ -e "/Applications/Google Chrome.app" ] && echo "==> Google Chrome est déjà installée" || brew cask install google-chrome
[ -e "/Applications/Slack.app" ] && echo "==> Slack est déjà installée" || brew cask install slack
[ -e "/Applications/Firefox.app" ] && echo "==> Firefox est déjà installée" || brew cask install firefox
[ -e "/Applications/OpenEmu.app" ] && echo "==> OpenEmu est déjà installée" || brew cask install openemu


## ************************* CONFIGURATION ********************************
echo "Configuration de quelques paramètres par défaut…"

## FINDER

# Affichage de la bibliothèque
chflags nohidden ~/Library

# Finder : affichage de la barre latérale / affichage par défaut en mode liste / affichage chemin accès / extensions toujours affichées
defaults write com.apple.finder ShowStatusBar -bool true
defaults write com.apple.finder FXPreferredViewStyle -string “Nlsv”
defaults write com.apple.finder ShowPathbar -bool true
sudo defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# Afficher le dossier maison par défaut
defaults write com.apple.finder NewWindowTarget -string "PfHm"
defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/"

# Recherche dans le dossier en cours par défaut
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

# Coup d'œîl : sélection de texte
defaults write com.apple.finder QLEnableTextSelection -bool true

# Pas de création de fichiers .DS_STORE
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true


## RÉGLAGES DOCK
# Taille du texte au minimum
defaults write com.apple.dock tilesize -int 40
# Agrandissement actif
defaults write com.apple.dock magnification -bool true
# Taille maximale pour l'agrandissement
defaults write com.apple.dock largesize -float 128


## APPS

# Safari : menu développeur / URL en bas à gauche / URL complète en haut / Do Not Track / affichage barre favoris
defaults write com.apple.safari IncludeDevelopMenu -int 1
defaults write com.apple.safari ShowOverlayStatusBar -int 1
defaults write com.apple.safari ShowFullURLInSmartSearchField -int 1
defaults write com.apple.safari SendDoNotTrackHTTPHeader -int 1
defaults write com.apple.Safari ShowFavoritesBar -bool true

# Photos : pas d'affichage pour les iPhone
defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool YES

# TextEdit : .txt par défaut
defaults write com.apple.TextEdit RichText -int 0

## ************ Fin de l'installation *********
echo "Finder et Dock relancés… redémarrage nécessaire pour terminer."
killall Dock
killall Finder

# echo "Derniers nettoyages…"
# brew cleanup
# rm -f -r /Library/Caches/Homebrew/*

echo "ET VOILÀ !"

fi
