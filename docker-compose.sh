#!/usr/bin/env bash

set -euo pipefail # Arrête le script en cas d'erreur ou de variable non définie

export VERBOSE=1

############################
#  docker-compose
############################

# Fonction pour obtenir la dernière version de Docker Compose depuis GitHub
get_latest_release() {
  local repo=$1
  local latest_version

  # Récupérer les informations sur la dernière release depuis l'API GitHub
  latest_version=$(curl --silent "https://api.github.com/repos/$repo/releases/latest" | \
    grep '"tag_name":' | \
    sed -E 's/.*"([^"]+)".*/\1/')

  # Vérifier si une version a été récupérée
  if [ -z "$latest_version" ]; then
    echo "Erreur : Impossible de récupérer la dernière version de $repo." >&2
    exit 1
  fi

  echo "$latest_version"
}

sudo rm -f /etc/bash_completion.d/docker-compose

# Désinstaller docker-compose
uninstall_dc() {
  echo "Désinstallation de docker-compose..."
  sudo rm -f /usr/local/bin/docker-compose /etc/bash_completion.d/docker-compose
}

# Installer docker-compose
install_dc() {
  local compose_version

  # Obtenir la dernière version de docker-compose
  compose_version=$(get_latest_release "docker/compose")
  echo "${compose_version} installation en cours..."

  # Télécharger et installer docker-compose
  sudo mkdir -p /etc/bash_completion.d/ /usr/local/bin/
  
  sudo curl -sSL "https://github.com/docker/compose/releases/download/${compose_version}/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
  
  sudo chmod +x /usr/local/bin/docker-compose

  echo "docker-compose ${compose_version} installé avec succès."
}

# Vérifier si docker-compose est déjà installé et à jour
if command -v docker-compose &>/dev/null; then
  echo "docker-compose existe !"
  
  # Récupérer la version installée
  compose_installed_version=$(docker-compose --version | grep -Po '(\d+\.\d+\.\d+)' || true)
  
  # Récupérer la dernière version disponible
  compose_latest_version=$(get_latest_release "docker/compose")

  if [ "$compose_installed_version" != "$compose_latest_version" ]; then
    echo "${compose_installed_version} installée. Mise à jour vers ${compose_latest_version}..."
    uninstall_dc
    install_dc
  else
    echo "docker-compose est déjà à jour (${compose_installed_version})."
  fi
else
  echo "docker-compose n'est pas installé. Installation en cours..."
  install_dc
fi

# Vérification finale
if command -v docker-compose &>/dev/null; then
  echo "docker-compose installé avec succès : $(docker-compose --version)"
else
  echo "Erreur : docker-compose n'a pas pu être installé." >&2
fi

