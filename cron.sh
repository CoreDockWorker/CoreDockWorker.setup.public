#!/usr/bin/env bash

############################
#  cron INSTALL
############################
# chargement en direct des éléments nécessaires tools
timestamp=`date +%Y%m%d%H%M%S`
wget -q https://gitlab.com/CoreDockWorker/CoreDockWorker.setup.public/raw/master/tools.inc.sh -O /tmp/.tools.${timestamp}.tmp && \
    source /tmp/.tools.${timestamp}.tmp && \
    rm /tmp/.tools.${timestamp}.tmp

if existing_command crontab; then
  echo 'crontab exists!'
else
  echo 'Your system does not have crontab'
  echo $DIST
  echo $DistroBasedOn
  declare -A PACKAGE_NAME
  PACKAGE_NAME["alpine"]="cron"
  PACKAGE_NAME["redhat"]="cron"
  PACKAGE_NAME["debian"]="cron"
  DistroUpdateCmd
  eval "$DistroInstallCmd ${PACKAGE_NAME[$DistroBasedOn]}"
fi
if ! existing_command crontab; then
  echo "ERROR: crontab is not installed" | tee "/tmp/error.setup.log"
  exit 1
fi

############################
#  crontab CONFIG
############################

systemctl start cron
systemctl enable cron
crontab -l
